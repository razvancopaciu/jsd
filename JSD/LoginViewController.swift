//
//  LoginViewController.swift
//  JSD
//
//  Created by Razvan Copaciu on 01/03/16.
//  Copyright © 2016 Razvan Copaciu. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet var usernameTextField: UITextField!
    @IBOutlet var passwordTextField: UITextField!
    @IBOutlet var loginButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func loginButtonTapped() {
        
        if (usernameTextField.text == "razvan") {
            if (passwordTextField.text == "password") {
                let firstVC = self.storyboard?.instantiateViewControllerWithIdentifier("TabBarController")
                self.presentViewController(firstVC!, animated: true, completion: nil)
            } else {
                let alertCtrl = UIAlertController(title: "Wrong password", message: "he password you entered does not match the username. Please try again.", preferredStyle: UIAlertControllerStyle.Alert);
                alertCtrl.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: {(action:UIAlertAction) in
                    self.passwordTextField.text = ""
                }));
                presentViewController(alertCtrl, animated: true, completion: nil);
            }
        } else {
            let alertController = UIAlertController(title: "Wrong username", message: "The username you entered does not exist. Please enter a valid username.", preferredStyle: .Alert)
            
            alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: {(action:UIAlertAction) in
                self.usernameTextField.text = ""
            }));
            
            presentViewController(alertController, animated: true, completion: nil)
        }
    }

}
