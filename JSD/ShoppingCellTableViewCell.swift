//
//  ShoppingCellTableViewCell.swift
//  JSD
//
//  Created by Razvan Copaciu on 19/02/16.
//  Copyright © 2016 Razvan Copaciu. All rights reserved.
//

import UIKit

class ShoppingCellTableViewCell: UITableViewCell {

    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var productDescriptionLabel: UILabel!
    @IBOutlet weak var productInCartImage: UIImageView!
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(presenter: ProductProtocol?) {
        self.setCellContent((presenter?.name)!, description: (presenter?.description)!, inCart: (presenter?.inCart)!)
    }
    
    func setCellContent(name: String, description: String, inCart:Bool) {
        self.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
        productNameLabel?.text = name
        if (!description.isEmpty) {
            productDescriptionLabel?.text = description
        }
        productInCartImage.hidden = !inCart
    }
}
