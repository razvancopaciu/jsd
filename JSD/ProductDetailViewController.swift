//
//  ProductDetailViewController.swift
//  JSD
//
//  Created by Razvan Copaciu on 19/02/16.
//  Copyright © 2016 Razvan Copaciu. All rights reserved.
//

import UIKit


class ProductDetailViewController: UIViewController {

    @IBOutlet var productName: UILabel!
    @IBOutlet var productPrice: UILabel!
    @IBOutlet var productDescription: UILabel!
    @IBOutlet var productAvailability: UILabel!
    
    @IBOutlet var changeProductPriceTextField: UITextField!
    @IBOutlet var changeProductAvailabilityTextField: UITextField!
    @IBOutlet var changeProductDescriptionTextField: UITextField!
    
    var productDetails: ProductViewModel!
    var products = [product]()
    var productIndex: NSInteger?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        productName.text! = productDetails.name
        productPrice.text! = productDetails.price
        if (!productDetails.description.isEmpty) {
            productDescription.text! = productDetails.description
        }
        productAvailability.text! = productDetails.availability
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func addToCartTapped(sender: UIButton) {
        shoppingList.addProductToShoppingList(productDetails.name, description: productDetails.description, price: productDetails.price, availability: productDetails.availability, inCart: true)
        productMgr.addToCartProductAtIndex(productIndex!)
    }
}
