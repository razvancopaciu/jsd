//
//  FirstViewController.swift
//  JSD
//
//  Created by Razvan Copaciu on 18/02/16.
//  Copyright © 2016 Razvan Copaciu. All rights reserved.
//

import UIKit

extension UIImage {
    func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage {
        
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight))
        image.drawInRect(CGRectMake(0, 0, newWidth, newHeight))
        let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
}

class FirstViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableViewTasks: UITableView!
    @IBOutlet weak var noProductsLabel: UILabel!
    var viewModel = ProductViewModel()
    var dataSource = [ProductViewModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Product list"
        productMgr.someHardcodedProducts()
        [rightTopBarButton()]
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        dataSource = []
        for prod in productMgr.products
        {
            viewModel.initWithProd(prod)
            dataSource.append(viewModel)
        }
        
        [tableViewTasks.reloadData()]
        if (dataSource.count > 0) {
            self.noProductsLabel.hidden = true
            self.tableViewTasks.hidden = false
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let productCell : ShoppingCellTableViewCell = tableView.dequeueReusableCellWithIdentifier("ProductCell") as! ShoppingCellTableViewCell
        productCell.configure(dataSource[indexPath.row])
        
        return productCell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let secondVC = self.storyboard?.instantiateViewControllerWithIdentifier("ProductDetailViewController") as? ProductDetailViewController
        secondVC?.productDetails = dataSource[indexPath.row]
        secondVC?.productIndex = indexPath.row
        navigationController?.pushViewController(secondVC!, animated: true)
        
        tableViewTasks.deselectRowAtIndexPath(indexPath, animated: true)
    }
       
    func rightTopBarButton() {
        let cartImage: UIImage = UIImage(named: "cart")!
        let shopButton : UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "cart"), style: UIBarButtonItemStyle.Plain, target: self, action: Selector("cartButtonTapped"))
        
        shopButton.image = shopButton.image?.resizeImage(cartImage, newWidth: 22)
        navigationItem.rightBarButtonItem = shopButton
    }
    
    func cartButtonTapped() {
        let secondVC = self.storyboard?.instantiateViewControllerWithIdentifier("ShoppingListViewController") as? ShoppingListViewController
        navigationController?.pushViewController(secondVC!, animated: true)
    }
}

