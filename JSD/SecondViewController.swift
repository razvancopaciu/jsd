//
//  SecondViewController.swift
//  JSD
//
//  Created by Razvan Copaciu on 18/02/16.
//  Copyright © 2016 Razvan Copaciu. All rights reserved.
//

import UIKit


class SecondViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var addProductTextField: UITextField!
    @IBOutlet weak var addProductPriceTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        [grayOutBackgroundImage()]
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func grayOutBackgroundImage() {
        let mask = UIView(frame: self.view.frame)
        mask.backgroundColor = UIColor.whiteColor().colorWithAlphaComponent(0.78)
        backgroundImage?.addSubview(mask)
    }
    
    @IBAction func addProductButtonTapped(sender: UIButton) {
        productMgr.addProduct(self.addProductTextField.text!, description:"", price:self.addProductPriceTextField.text!, availability: "Yes", inCart:false);
        addProductTextField.text = ""
        addProductPriceTextField.text = ""
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        return true;
    }
}

