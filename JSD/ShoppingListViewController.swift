//
//  ShoppingListViewController.swift
//  JSD
//
//  Created by Razvan Copaciu on 19/02/16.
//  Copyright © 2016 Razvan Copaciu. All rights reserved.
//

import UIKit

class ShoppingListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var total: UILabel!
    @IBOutlet weak var shoppingListLabel: UILabel!
    var viewModel = ProductViewModel()
    var dataSource = [ProductViewModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        for prod in shoppingList.products
        {
            viewModel.initWithProd(prod)
            dataSource.append(viewModel)
        }
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        [self.checkIfListIsEmpty()]
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
     func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    
     func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if (editingStyle == UITableViewCellEditingStyle.Delete) {
            dataSource.removeAtIndex(indexPath.row)
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Automatic)
            [self.checkIfListIsEmpty()]
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell : UITableViewCell = UITableViewCell (style: UITableViewCellStyle.Subtitle, reuseIdentifier: "ShoppingCell")
        
        cell.textLabel?.text = dataSource[indexPath.row].name;
        if (!dataSource[indexPath.row].description.isEmpty) {
            cell.detailTextLabel?.text = dataSource[indexPath.row].price
        }
        
        return cell
    }
    
    func showTotalPrice() {
        var sum: Double = 0
        for (var i = 0; i < dataSource.count; i++) {
            let price: String = dataSource[i].price
            sum = sum + Double(price)!
        }
        total.text! = "Total: "
        total.text! = (total.text?.stringByAppendingString(String(sum)))!
    }
    
    func checkIfListIsEmpty() {
        if (dataSource.count > 0) {
            shoppingListLabel.text! = "Shopping list:"
            [showTotalPrice()]
        } else {
            shoppingListLabel.textAlignment = NSTextAlignment.Center
            shoppingListLabel.text! = "Shopping list is empty"
            total.hidden = true
        }
    }
    
}
