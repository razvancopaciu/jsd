//
//  TaskManager.swift
//  JSD
//
//  Created by Razvan Copaciu on 18/02/16.
//  Copyright © 2016 Razvan Copaciu. All rights reserved.
//

import UIKit

var productMgr:TaskManager = TaskManager()
var shoppingList:TaskManager = TaskManager()

protocol ProductProtocol {
    var name: String { get }
    var description: String { get }
    var price: String { get }
    var availability: String { get }
    var inCart: Bool { get }
}

struct ProductViewModel: ProductProtocol {
    var productName = "No name"
    var productDescription = "No description"
    var productPrice: String = "Free"
    var productAvailability = "Yes"
    var productInCart: Bool = false
    
    var name: String { return productName }
    var description: String { return productDescription }
    var price: String { return productPrice }
    var availability: String { return productAvailability }
    var inCart: Bool { return productInCart }
    
    mutating func initWithProd(prod: product) {
        productName = prod.name
        productDescription = prod.description
        productPrice = prod.price
        productAvailability = prod.availability
        productInCart = prod.inCart
    
    }
}

struct product {
    var name = "No name"
    var description = "No description"
    var price: String = "Free"
    var availability = "Yes"
    var inCart: Bool = false
}

class TaskManager: NSObject {
    
    var products = [product]()
    
    func someHardcodedProducts() -> Array<product>  {
        
        products.append((product(name: "Coca-Cola", description: "Diet coke, 2 L", price: "4.99", availability: "Yes", inCart: false)))
        products.append((product(name: "Fanta", description: "Orange, 0.5 L", price: "2.99", availability: "No", inCart: false )))
        products.append((product(name: "Pizza", description: "Quatro Sttagioni", price: "14.99", availability: "Yes", inCart: false)))
        products.append((product(name: "Bread", description: "Toast, white", price: "8.49", availability: "Yes", inCart: false)))
        products.append((product(name: "Milka", description: "with Oreo", price: "7.29", availability: "Yes", inCart: false)))
        products.append((product(name: "Doritos", description: "with Cheese", price: "2.23", availability: "Yes", inCart: false)))
        
        return products
    }
    
    func addProduct(name: String, description: String, price: String, availability: String, inCart: Bool) {
        products.append(product(name: name, description: description, price: price, availability: availability, inCart: inCart))
    }
    
    func addToCartProductAtIndex(index: NSInteger) {
        products[index].inCart = true;
    }
    
    func removeFromCartProductAtIndex(index : NSInteger) {
        products[index].inCart = false;
    }
    
    func addProductToShoppingList(name: String, description: String, price: String, availability: String, inCart: Bool) {
        products.append(product(name: name, description: description, price: price, availability: availability, inCart: inCart))
    }
}
